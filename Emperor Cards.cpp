#include <iostream>
#include <string>
#include <ctime>

using namespace std;

struct List {
	string card;
	List* next = NULL;
};

struct Node {
	string cards;
	Node* next = NULL;
	Node* prev = NULL;
};

Node *createKaijiDeck(int round);
Node *selectKaijiCard(Node *head, int index);
Node *deleteSelectedKaijiCard(Node *head, int index);
List* createTonegawaDeck(int round);
List *selectTonegawaCard(List *root, int indexT);
List *deleteSelectedTonegawaCard(List *root, int indexT);
bool betCheck(int betLength, int remainingLength);
int prizeCheck(int betLength, int round);
void printDeck(Node * head);
int winCheck(List *root, Node *head, int cardIndex, int randTonegawa);
void gameOver(int cash, int round, int input, int remaining);

Node *createKaijiDeck(int round)
{
	Node* head = NULL;
	Node* curr = NULL;
	Node* prev = NULL;

	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9)
	{
		curr = new Node;
		curr->cards = "Emperor";
		prev = curr;
		head = curr;

		for (int i = 0; i < 3; i++)
		{

			curr = new Node;
			curr->cards = "Citizen";
			prev->next = curr;
			prev = curr;
		}

		curr = new Node;
		curr->cards = "Citizen";
		prev->next = curr;

	}
	if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12)
	{
		curr = new Node;
		curr->cards = "Slave";
		prev = curr;
		head = curr;

		for (int i = 0; i < 3; i++)
		{
			curr = new Node;
			curr->cards = "Citizen";
			prev->next = curr;
			prev = curr;
		}
		curr = new Node;
		curr->cards = "Citizen";
		prev->next = curr;
	}
	return head;
}
Node *selectKaijiCard(Node *head, int indexK) {

	for (int i = 0; i < indexK - 1; i++) {

		head = head->next;
	}

	return head;
}
Node *deleteSelectedKaijiCard(Node *head, int indexK) {

	Node *curr = NULL;
	if (indexK == 1)
	{
		curr = head;
		head = head->next;
		delete curr;
		return head;
	}

	Node *deleteCard = head;

	for (int i = 0; i < indexK - 1; i++) {

		curr = deleteCard;
		deleteCard = deleteCard->next;
	}
	curr->next = deleteCard->next;
	delete deleteCard;
	return head;
}
List *createTonegawaDeck(int round) {

	List *root = NULL;
	List *front = NULL;
	List *tail = NULL;

	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9) {
		front = new List;
		front->card = "Slave";
		tail = front;
		root = front;

		for (int i = 0; i < 3; i++) {

			front = new List;
			front->card = "Citizen";
			tail->next = front;
			tail = front;
		}

		front = new List;
		front->card = "Citizen";
		tail->next = front;
	}

	if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12) {
		front = new List;
		front->card = "Emperor";
		tail = front;
		root = front;

		for (int i = 0; i < 3; i++) {

			front = new List;
			front->card = "Citizen";
			tail->next = front;
			tail = front;
		}

		front = new List;
		front->card = "Citizen";
		tail->next = front;
		front->next = root;
	}

	return root;
}
List *selectTonegawaCard(List *root, int indexT) {
	for (int i = 0; i < indexT; i++) {

		root = root->next;
	}

	return root;

}
List *deleteSelectedTonegawaCard(List *root, int indexT) {

	List *front = NULL;
	if (indexT == 0)
	{
		front = root;
		root = root->next;
		delete front;
		return root;
	}

	List *deleteTonegawaCard = root;

	for (int i = 0; i < indexT; i++) {

		front = deleteTonegawaCard;
		deleteTonegawaCard = deleteTonegawaCard->next;
	}
	front->next = deleteTonegawaCard->next;
	delete deleteTonegawaCard;
	return root;
}
bool betCheck(int betLength, int remainingLength) {

	bool flag;
	if (betLength <= remainingLength) {
		flag = true;
	}
	else if (betLength > remainingLength) {
		flag = false;
	}
	return flag;
}
int prizeCheck(int betLength, int round) {

	int yen;
	if (round == 1 || round == 2 || round == 3 || round == 7 || round == 8 || round == 9) {

		yen = betLength * 100000;
	}
	if (round == 4 || round == 5 || round == 6 || round == 10 || round == 11 || round == 12) {

		yen = betLength * 500000;
	}
	return yen;
}
void printDeck(Node *head) {

	int count = 1;
	cout << "===============" << endl;
	cout << "Kaiji's cards" << endl;
	cout << "===============" << endl << endl;
	while (head != NULL) {
		cout << count << ". " << head->cards << endl;
		head = head->next;
		count++;
	}
	cout << endl;
}
int winCheck(List *root, Node *head, int cardIndex, int randTonegawa) {

	int result = 0;

	if ((head->cards == "Citizen") && (root->card == "Citizen")) 
	{
		result = 0;
	}
	else if ((head->cards == "Emperor") && (root->card == "Citizen") || (head->cards == "Slave") && (root->card == "Emeperor") || (head->cards == "Citizen") && (root->card == "Slave")) 
	{
		result = 1;
	}
	else if ((root->card == "Emperor") && (head->cards == "Citizen") || root->card == "Slave" && head->cards == "Emperor" || root->card == "Citizen" && head->cards == "Slave") 
	{	
		result = 2;
	}
	head = deleteSelectedKaijiCard(head, cardIndex);
	root = deleteSelectedTonegawaCard(root, randTonegawa);
	return result;
}
void gameOver(int cash, int round, int input, int remaining) {

	if (round <= 12 && cash == 20000000 && betCheck(input, remaining) == true) {
		cout << "You did not entirely win! You only got " << cash << " Yen in 12 rounds!" << endl;
		system("pause");
		exit(0);
	}
	else if (round == 12 && cash < 20000000 && betCheck(input, remaining) == false) {

		cout << "You won! You got" << cash << " Yen at Round " << round << endl;
	}

}

int main() {

	// Seed the RNG
	srand((unsigned int)time(0));

	// Initilizing variables
	int input, cardIndex, randTonegawa, countTonegawa = 0, cash = 0, remaining = 30;

	// Round loop
	for (int round = 1; round < 12; round++) {

		cout << "===============================================" << endl;
		cout << "                ROUND " << round << endl;
		cout << "===============================================" << endl;
		cout << "How much would you like to bet, in milimeters?" << endl;
		cout << "(You still have " << remaining << " milimeters left.)" << endl;
		cin >> input;
		betCheck(input, remaining);

		// Match loop
		if (betCheck(input, remaining) == true) {

			cout << "You can win " << prizeCheck(input, round) << " Yen." << endl << endl;
			cout << "Cash currently at hand: " << cash << endl;
			Node* head = createKaijiDeck(round);
			List* root = createTonegawaDeck(round);
			do {
				printDeck(head);

				cout << "Select a card to play [1 - 5]: ";
				cin >> cardIndex;
				randTonegawa = (rand() % (5 - countTonegawa));
				Node* selectKaijiCardAtIndex = selectKaijiCard(head, cardIndex);
				cout << "You chose the card: " << selectKaijiCardAtIndex->cards << endl;
				List* selectTonegawaCardAtIndex = selectTonegawaCard(root, randTonegawa);
				cout << "Tonegawa chose the card: " << selectTonegawaCardAtIndex->card << endl;
				cout << endl;
				countTonegawa++;
			} while (winCheck(root, head, cardIndex, randTonegawa) == 0);

			// Match up checker (Emperor > Citizen > Slave > Emperor)
			if (winCheck(root, head, cardIndex, randTonegawa) == 1) {

				cash = cash + prizeCheck(input, round);
				cout << "Round " << round << " winner is Kaiji." << endl;
				cout << "You won " << prizeCheck(input, round) << " Yen!" << endl;
			}
			else if (winCheck(root, head, cardIndex, randTonegawa) == 2) {

				remaining = remaining - input;
				cout << "Round " << round << " winner is Tonegawa." << endl;
				cout << "The pin moved by " << input << " milimeters!" << endl;
			}

		}
		else if (betCheck(input, remaining) == false)
		{
			cout << "You lose! You already lost your ear!" << endl;
			system("pause");
			exit(0);
		}
	}
	return 0;
}

