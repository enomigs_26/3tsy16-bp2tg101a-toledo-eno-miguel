#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

int main()
{
	int credits = 99;
	int itemprice;
	int decision = 'y';
	int packdecision;
	int rebuydecision;

	while (credits > 0)
	{
		cout << "Remaining credits: " << credits << endl;
		cout << "Price of item to buy: ";
		cin >> itemprice;
		if (credits > itemprice)
		{
			credits = credits - itemprice;
		}
		else
		{
			cout << "You don't have enough credits to buy this item." << endl;
			cout << "Buy " << "something here" << "credits package? *Recommended (Y) yes / (N) No" << endl;
			cin >> packdecision;
			if (packdecision == 'y')
			{
				//credits = credits + something here
				cout << "You now have " << credits << "credits in your hand." << endl;
				cout << "Continue buying the previous item worth " << itemprice << "credits (Y) Buy (N) Don't buy" << endl;
				cin >> rebuydecision;
				if (rebuydecision == 'y')
				{
					credits = credits - itemprice;
					cout << "Item bought successfully" << endl;
					cout << "Remaining credits: " << credits << endl;
				}
				else
				{
					cout << "Thank you for shopping :)" << endl;
					system("pause");
					return 0;
				}
			}
			else
			{
				cout << "Thank you for shopping :)" << endl;
				system("pause");
				return 0;
			}
		}
	}

	system("pause");
	return 0;
}

void packagerray()
{
	const int x = 7;
	int packages[x] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };

	for (int i = 0; i < x - 1; i++)
	{
		int p = i; 
		for (int j = i + 1; j < x; j++) 
		{
			if (packages[j] < packages[p])
			{
				p = j;
			}
		}

		if (p == i) continue;
		int temp = packages[i];
		packages[i] = packages[p];
		packages[p] = temp;
	}
}