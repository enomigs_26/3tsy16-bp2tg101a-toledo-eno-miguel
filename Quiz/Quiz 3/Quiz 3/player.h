#pragma once
#include <string>

using namespace std;

class player
{
public:
	player(string Name, string Class, int pow, int vit, int dex, int agi);
	~player();

	std::string getName();
	void setName(std::string);
	string getClass();
	int getMaxHp();		
	int getHp();
	int getPow();
	int getVit();
	int getAgi();
	int getHitrate();
	void setDamage();
	void setName(string Name);
	void setClass(string Class);
	void printStats();
	void takeDamage(int damage);
	void fight(player *target);
	void hitrate(player *enemy);
	void recover();
	void win();

private:
	std::string playerName;
	string pClass;
	int pHp;
	int pMaxHp;
	int pPow;
	int pVit;
	int pDex;
	int pAgi;
	int pHitrate;
	int pDamage;
};


