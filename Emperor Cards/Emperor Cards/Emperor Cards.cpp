#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <vector>

using namespace std;

void printRound(int &round, int &money, string side)
{
	cout << "===============================================" << endl;
	cout << "                ROUND " << round << endl;
	cout << "===============================================" << endl;
	cout << "Money in hand: " << money << endl;
	cout << "Side: " << side << endl;

}
void bet(int &mm, int remainingDistance, string &result)
{
	while (mm <= 0 || mm > remainingDistance)
	{
		cout << "How much would you like to bet, in milimeters?" << endl;
		cout << "(You still have " << remainingDistance << " milimeters left.)" << endl;
		cin >> mm;
	}
}
void printDeck(int &remainingCards, string side, vector<string> EmperorDeck, vector<string> SlaveDeck, string &result)
{
	if (result != "Draw")
	{
		cout << "===============" << endl;
		cout << "Kaiji's cards" << endl;
		cout << "===============" << endl << endl;
		if (side == "Emperor")
		{
			for (int i = 0; i < remainingCards; i++)
			{
				cout << " [" << i << "]" << EmperorDeck[i] << endl;
			}
		}
		if (side == "Slave")
		{
			for (int i = 0; i < remainingCards; i++)
			{
				cout << " [" << i << "]" << SlaveDeck[i] << endl;
			}
		}
	}
}
void printDeck2(int &remainingCards, string side, vector<string> EmperorDeck, vector<string> SlaveDeck, string &result)
{
	if (result == "Draw")
	{
		cout << "===============" << endl;
		cout << "Kaiji's cards" << endl;
		cout << "===============" << endl << endl;
		if (side == "Emperor")
		{
			for (int i = 0; i < remainingCards; i++)
			{
				cout << " [" << i << "]" << EmperorDeck[i] << endl;
			}
		}
		if (side == "Slave")
		{
			for (int i = 0; i < remainingCards; i++)
			{
				cout << " [" << i << "]" << SlaveDeck[i] << endl;
			}
		}
	}
}
void playround(int &remainingCards, int &choice, string &result, string side, vector<string> EmperorDeck, vector<string> SlaveDeck)
{

	srand(time(0));
	while (result == "Draw")
	{
		printDeck2(remainingCards, side, EmperorDeck, SlaveDeck, result);
		int enemy = rand() % remainingCards;
		cout << " " << endl;
		cout << "Pick a card" << endl;
		cin >> choice;
		cout << " " << endl;

		if (side == "Emperor")
		{
			cout << "OPEN!" << endl;
			cout << "Player: " << EmperorDeck[choice] << " vs. " << "Enemy: " << SlaveDeck[enemy] << endl;
			if (EmperorDeck[choice] == "Emperor" && SlaveDeck[enemy] == "Civilian")
			{
				cout << "You Win!" << endl;
				result = "Win";
			}
			else if (EmperorDeck[choice] == "Emperor" && SlaveDeck[enemy] == "Slave")
			{
				cout << "You Lose!" << endl;
				result = "Lose";
			}
			else if (EmperorDeck[choice] == "Civilian" && SlaveDeck[enemy] == "Civilian")
			{
				cout << "DRAW!" << endl;
				result = "Draw";
				EmperorDeck.erase(EmperorDeck.begin() + choice);
				SlaveDeck.erase(SlaveDeck.begin() + enemy);
			}
			else if (EmperorDeck[choice] == "Civilian" && SlaveDeck[enemy] == "Slave")
			{
				cout << "You Win!" << endl;
				result = "Win";
			}

		}
		if (side == "Slave")
		{
			cout << "OPEN!" << endl << endl;
			cout << "Player: " << SlaveDeck[choice] << " vs. " << "Enemy: " << EmperorDeck[enemy] << endl;
			if (SlaveDeck[choice] == "Slave" && EmperorDeck[enemy] == "Civilian")
			{
				cout << "You Lose!" << endl;
				result = "Lose";
			}
			else if (SlaveDeck[choice] == "Civilian" && EmperorDeck[enemy] == "Emperor")
			{
				cout << "You Lose!" << endl;
				result = "Lose";
			}
			else if (SlaveDeck[choice] == "Civilian" && EmperorDeck[enemy] == "Civilian")
			{
				cout << "DRAW!" << endl;
				result = "Draw";
				EmperorDeck.erase(EmperorDeck.begin() + choice);
				SlaveDeck.erase(SlaveDeck.begin() + enemy);
			}
			else if (SlaveDeck[choice] == "Slave" && SlaveDeck[enemy] == "Emperor")
			{
				cout << "You Win!" << endl;
				result = "Win";
			}

		}
		system("pause");
		system("cls");
		remainingCards -= 1;
	}
	remainingCards = 5;
	EmperorDeck.clear();
	SlaveDeck.clear();
}
void payout(int &choice, int &money, int mm, int &remainingDistance, string result, string side)
{
	int pay = 0;
	if (result == "Win")
	{
		if (side == "Emperor")
		{
			pay = mm * 100000;
		}
		else if (side == "Slave")
		{
			pay = mm * 500000;
		}
		cout << "You received " << pay << " Yen" << endl;
		money += pay;

	}
	else if (result == "Lose")
	{
		cout << "Director: Drill... Drill... Drill..." << endl;
		cout << "Kaiji: AHHHHHHHHHHHHHHHHHHHHHHH!" << endl;
		cout << "The pin moved by " << mm << " mm" << endl;
		remainingDistance -= mm;
	}
}
void roundChecker(int &round, string &side)
{
	if (round == 3)
	{
		cout << "=====================================" << endl;
		cout << "You are now playing on the Slave side" << endl;
		cout << "=====================================" << endl << endl;
		side = "Slave";
	}
	if (round == 6)
	{
		cout << "=======================================" << endl;
		cout << "You are now playing on the Emperor side" << endl;
		cout << "=======================================" << endl << endl;
		side = "Emperor";
	}
	if (round == 9)
	{
		cout << "=====================================" << endl;
		cout << "You are now playing on the Slave side" << endl;
		cout << "=====================================" << endl << endl;
		side = "Slave";
	}
}
void endResult(int money, int remainingDistance, int round)
{
	if (remainingDistance > 0 && round > 12)
	{
		if (money >= 20000000)
		{
			cout << "You Won!! You reached you 20,000,000 yen goal!" << endl;
		}
		else if (money < 20000000)
		{
			cout << "You still have your ear but 20,000,000 was out of reach." << endl;
		}
	}
	if (remainingDistance <= 0)
	{
		cout << "You've lost your ear and the game" << endl;
	}
}

int main()
{
	int mm;
	int money = 0;
	int remainingDistance = 30;
	int remainingCards = 5;
	int round = 1;
	int choice;
	string side = "Emperor";

	vector<string> EmperorDeck;
	EmperorDeck.push_back("Emperor");
	EmperorDeck.push_back("Civilian");
	EmperorDeck.push_back("Civilian");
	EmperorDeck.push_back("Civilian");
	EmperorDeck.push_back("Civilian");
	vector<string> SlaveDeck;
	SlaveDeck.push_back("Slave");
	SlaveDeck.push_back("Civilian");
	SlaveDeck.push_back("Civilian");
	SlaveDeck.push_back("Civilian");
	SlaveDeck.push_back("Civilian");

	while (round < 13 && remainingDistance != 0)
	{
		string result = "Draw";
		while (result == "Draw")
		{
			printRound(round, money, side);
			bet(mm, remainingDistance, result);
			printDeck(remainingCards, side, EmperorDeck, SlaveDeck, result);
			playround(remainingCards, choice, result, side, EmperorDeck, SlaveDeck);
			payout(choice, money, mm, remainingDistance, result, side);
		}
		roundChecker(round, side);
		round++;
		mm = 0;
		system("pause");
		system("cls");
	}
	endResult(money, remainingDistance, round);
	system("pause");
	return 0;
}