#include "Skills.h"

skills::skills(string name)
{
	mName = name;
}

string skills::getName()
{
	return mName;
}

int skills::getMpCost()
{
	return mMpCost;
}

bool skills::willActivate(vector<unit*> character)
{
	if (character.at(0)->getCurrentMp() < mMpCost)
		return false;
	else
		return true;
}

void skills::activateAbility(vector<unit*> character, vector<unit*> target)
{
}
