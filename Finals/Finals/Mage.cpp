#include <iostream>
#include "Unit.h"
#include "Mage.h"

using namespace std;

mage::mage(string name, string side) : unit()
{
	mSide = side;
	mName = name;
	mClass = "Mage";
	mHp = 150;
	mMaxHp = mHp;
	mMp = 200;
	mPow = 40;
	mVit = 10;
	mAgi = rand() % 6 + 1;
	mDex = 30;
}
