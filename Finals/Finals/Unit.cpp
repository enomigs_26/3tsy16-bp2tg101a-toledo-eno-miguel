#include "Unit.h"
#include <string>
#include <iostream>
#include <ctime>
#include "Unit.h"

using namespace std;


unit::unit()
{
	mClass = " ";
	mHp = 0;
	mPow = 0;
	mVit = 0;
	mAgi = 0;
	mDex = 0;
}

unit::~unit()
{
	cout << "Unit is dead." << endl;
}

unit::unit(string name, string Class)
{
	mClass = Class;
}

string unit::getName()
{
	return mName;
}

string unit::getClass()
{
	return mClass;
}

int unit::getHp()
{
	return mHp;
}

int unit::getMaxHp()
{
	return mMaxHp;
}

int unit::getMp()
{
	return mMp;
}

int unit::getCurrentMp()
{
	return mCurrentMp;
}

int unit::getPow()
{
	return mPow;
}

int unit::getVit()
{
	return mVit;
}

int unit::getAgi()
{
	return mAgi;
}

int unit::getDex()
{
	return mDex;
}

int unit::getDamage()
{
	return mDamage;
}

int unit::getMpCost()
{
	return mMpCost;
}

void unit::printStats()
{
	cout << "Name: " << mName << endl;
	cout << "Class: " << mClass << endl;
	cout << "HP: " << mHp << "/" << mMaxHp << endl;
	cout << "MP: " << mMp << endl;
	cout << "Pow: " << mPow << endl;
	cout << "Vit: " << mVit << endl;
	cout << "Agi: " << mAgi << endl;
	cout << "Dex: " << mDex << endl;
	cout << "========================" << endl;
}

void unit::attack(vector<unit*> &unit, int choice)
{
	srand(time(0));

	int critChance = rand() % 5 + 1;
	int randPow = ((rand() % this->getPow() * 0.2) + this->getPow());
	int baseDamage = randPow * 1; 
	int damage = (baseDamage - unit[choice]->getVit()); 

	cout << this->getName() << " attacks " << unit[choice]->getName() << endl;

	if (mClass == "Warrior" && unit[choice]->getClass() == "Assassin" || mClass == "Assassin" && unit[choice]->getClass() == "Mage" || mClass == "Mage" && unit[choice]->getClass() == "Warrior") 
	{
		if (critChance == 5)
		{
			cout << "Critical Damage!" << endl;
			unit[choice]->takeDamage((mDamage * 1.5) * 1.5);
		}
		else
		{
			unit[choice]->takeDamage(damage *1.5); 
		}
	}
	else if (mClass == "Assassin" && unit[choice]->getClass() == "Warrior" || mClass == "Mage" && unit[choice]->getClass() == "Assassin" || mClass == "Warrior" && unit[choice]->getClass() == "Mage")
	{
		if (critChance == 5)
		{
			cout << "Critical Damage!" << endl;
			unit[choice]->takeDamage((damage * 0.5) * 1.5);
		}

		else
		{
			unit[choice]->takeDamage(damage * 0.5);
		}
	}

	else
	{
		if (critChance == 5)
		{
			cout << "Critical Damage!" << endl;
			unit[choice]->takeDamage(damage* 1.5);
		}

		else
		{
			unit[choice]->takeDamage(damage);
		}
	}
}

int unit::getHitRate(vector<unit*>& target, int choice)
{
	mHitRate = (mDex / target[choice]->getAgi()) * 100;

	if (mHitRate > 80)
	{
		mHitRate = 80;
		return mHitRate;
	}

	else if (mHitRate < 20)
	{
		mHitRate = 20;
		return mHitRate;
	}

	else { return mHitRate; }
}

bool unit::willHit(vector<unit*>& target, int choice)
{
	srand(time(0));

	int hitPercent = rand() % 100 + 1;

	if (target[choice]->getHitRate(target, choice) < hitPercent) { return false; }
	else {return true;}
}

void unit::takeDamage(int damage)
{
	mHp -= damage; 

	cout << "Damage taken: " << damage << endl;

	if (mHp < 0) mHp = 0;
}

float unit::dmgMultiplier(vector<unit*> &target)
{
	if (mClass == "Warrior" && target[0]->mClass == "Assassin" || mClass == "Assassin" && target[0]->mClass == "Mage" || mClass == "Mage" && target[0]->mClass == "Warrior")
	{
		return 1.5f;
	}
	else if
		(mClass == "Assassin" && target[0]->mClass == "Warrior" || mClass == "Mage" && target[0]->mClass == "Assassin" || mClass == "Warrior" && target[0]->mClass == "Mage") 
	{
		return 0.5f;
	}
	else
	{
		return 1.0f;
	}
}

void unit::useMp(int mp)
{
	mMp -= mp;

	if (mMp < 0) mMp = 0;
}

string unit::getSide()
{
	return mSide;
}

//int unit::useSkill()
//{
	//return ;
//}





