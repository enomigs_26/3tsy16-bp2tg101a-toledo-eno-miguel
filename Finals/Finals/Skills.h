#pragma once
#include "Unit.h"
#include <vector>

using namespace std;

class unit;
class skills
{
public:
	skills();
	skills(string name);
	string getName();
	int getMpCost();
	bool willActivate(vector<unit*> character);
	virtual void activateAbility(vector<unit*> character, vector<unit*> target);

protected:
	string mName;
	int mMpCost;
};
