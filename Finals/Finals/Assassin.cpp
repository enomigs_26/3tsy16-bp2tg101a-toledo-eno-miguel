#include <iostream>
#include "Unit.h"
#include "Assassin.h"

using namespace std;

assassin::assassin(string name, string side) : unit()
{
	mSide = side;
	mName = name;
	mClass = "Assassin";
	mHp = 100;
	mMaxHp = mHp;
	mMp = 100;
	mPow = 30;
	mVit = 15;
	mAgi = rand() % 6 + 1;
	mDex = 20;
}
