#include <iostream>
#include "Unit.h"
#include "Warrior.h"

using namespace std;

warrior::warrior(string name, string side) : unit()
{
	mSide = side;
	mName = name;
	mClass = "Warrior";
	mHp = 200;
	mMaxHp = mHp;
	mMp = 50;
	mPow = 20;
	mVit = 30;
	mAgi = rand() % 6 + 1;
	mDex = 20;
}

