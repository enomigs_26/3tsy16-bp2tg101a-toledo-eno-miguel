#pragma once
#include <string>
#include <vector>

using namespace std;

class unit
{
public:
	unit();
	~unit();
	unit(string name, string Class);
	string getName();
	string getClass();
	int getHp();
	int getMaxHp();
	int getMp();
	int getCurrentMp();
	int getPow();
	int getVit();
	int getAgi();
	int getDex();
	int getDamage();
	int getMpCost();
	void printStats();
	void attack(vector<unit*> &unit, int choice);
	int getHitRate(vector<unit*> &target, int choice);
	int getSkillDamage(vector<unit*> &target, float damageCoef);
	bool willHit(vector<unit*> &target, int choice);
	void takeDamage(int damage);
	float dmgMultiplier(vector<unit*> &target);
	void useMp(int mp);
	string getSide();
	

protected:
	string mName;
	string mClass;
	string mSide;
	int mHp;
	int mMaxHp;
	int mMp;
	int mCurrentMp;
	int mPow;
	int mVit;
	int mAgi;
	int mDex;
	int mDamage;
	int mMpCost;
	int mHitRate;
	
};

