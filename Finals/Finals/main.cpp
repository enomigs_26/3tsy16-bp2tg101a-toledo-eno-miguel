#include <iostream>
#include <string>
#include <vector>
#include <ctime>
#include "Unit.h"
#include "Warrior.h"
#include "Assassin.h"
#include "Mage.h"

using namespace std;

void playerSummon(vector<unit*> &playerParty)
{
	unit* person = new warrior("Greystone", "Pasado");
	unit* person2 = new assassin("Ezio", "Pasado");
	unit* person3 = new mage("Veigar", "Pasado");

	playerParty.push_back(person);
	playerParty.push_back(person2);
	playerParty.push_back(person3);

}
void enemySummon(vector<unit*> &enemyParty)
{
	unit* enemy = new warrior("Lockhearth", "Bagsak");
	unit* enemy2 = new assassin("Grim.exe", "Bagsak");
	unit* enemy3 = new mage("Gideon", "Bagsak");

	enemyParty.push_back(enemy);
	enemyParty.push_back(enemy2);
	enemyParty.push_back(enemy3);

}
void playerParty(vector<unit*> &playerParty)
{
	
	for (int i = 0; i < 3; i++)
	{
		playerParty[i]->printStats();
	}

}
void enemyParty(vector<unit*> &enemyParty)
{

	for (int i = 0; i < 3; i++)
	{
		enemyParty[i]->printStats();
	}
}
void combineParty(vector<unit*> &player, vector<unit*> &enemy, vector<unit*> &characters)
{
	for (int i = 0; i < player.size(); i++)
	{
		characters.push_back(player[i]);
	}
	for (int j = 0; j < enemy.size(); j++)
	{
		characters.push_back(enemy[j]);
	}
}
void turns(vector<unit*> &characters)
{
	int size = characters.size();
	int number = 1;

	unit* temp;

	for (int i = 0; i < size; i++)
	{
		for (int j = i + 1; j < size; j++)
		{
			if (characters[j]->getAgi() > characters[i]->getAgi())
			{
				temp = characters[i];
				characters[i] = characters[j];
				characters[j] = temp;
			}
		}
	}
}
void printOrder(vector<unit*> characters)
{
	int number = 1;
	cout << "========================" << endl;
	cout << "       Turn Order       " << endl;
	cout << "========================" << endl;
	for (int i = 0; i < characters.size(); i++)
	{
		cout << "[" << number << "] " << "[" << characters[i]->getSide() << "] " << characters[i]->getName() << " [" << characters[i]->getAgi() << "]" << endl;
		number++;
	}

	cout << endl;
	cout << characters.at(0)->getName() << " is now going to attack." << endl;
}
void playerAttack(vector<unit*> &player, vector<unit*> &enemy, vector<unit*> &characters, int userChoice)
{
	int playerTarget;
	int number = 0;

	cout << "========================" << endl;
	cout << "    Choose to attack    " << endl;
	cout << "========================" << endl;

	for (int i = 0; i < enemy.size(); i++)
	{
		cout << "[" << number << "] " << enemy[i]->getName() << " [" << enemy[i]->getHp() << "]" << endl;
		number++;
	}

	cout << "Target: ", cin >> playerTarget;

	cout << "========================" << endl;
	cout << "        Attacking       " << endl;
	cout << "========================" << endl;
	if (userChoice == 1)
	{
		if (characters.at(0)->willHit(enemy, playerTarget) == false) { characters.at(0)->attack(enemy, playerTarget); }
		else { cout << characters.at(0)->getName() << " attack miss..." << endl; }
	}

	else if (userChoice == 2)
	{
		cout << "Wait lang" << endl;
	}
}
void enemyAttack(vector<unit*> &player, vector<unit*> &enemy, vector<unit*> &characters, int enemyChoice, int enemyTarget)
{
	for (int i = 0; i < player.size(); i++)
	{
		player[i];
	}

	cout << "========================" << endl;
	cout << "        Attacking       " << endl;
	cout << "========================" << endl;

	if (enemyChoice == 1)
	{
		if (characters.at(0)->willHit(player, enemyTarget) == false) { characters.at(0)->attack(player, enemyTarget); }
		else { cout << characters.at(0)->getName() << " attack miss..." << endl; }
	}

	else if (enemyChoice == 2)
	{
		cout << "Wait lang" << endl;
	}
}
void checkPlayerHP(vector<unit*> &player)
{
	for (int i = 0; i < player.size(); i++)
	{
		if (player[i]->getHp() == 0)
		{
			player.erase(player.begin() + i);
		}
	}
}
void checkEnemyHP(vector<unit*> &enemy)
{
	for (int i = 0; i < enemy.size(); i++)
	{
		if (enemy[i]->getHp() == 0)
		{
			enemy.erase(enemy.begin() + i);
		}
	}
}
void checkAllHP(vector<unit*> &characters)
{
	for (int i = 0; i < characters.size(); i++)
	{
		if (characters[i]->getHp() == 0)
		{
			characters.erase(characters.begin() + i);
		}
	}
}
void afterTurn(vector<unit*> &characters)
{
	int i = 0;
	unit* temp;
	for (int j = i + 1; j < characters.size(); j++)
	{
		temp = characters[i];
		characters[i] = characters[j];
		characters[j] = temp;
		i++;
	}
	for (int i = 0; i < characters.size(); i++)
	{
		characters[i];
	}
}
void ending(vector<unit*> &player, vector<unit*> &enemy)
{
	cout << "========================" << endl;
	cout << "     Team Pasado        " << endl;
	cout << "========================" << endl;

	if (player.size() == 0) // If Player Team Wiped Out
	{
		cout << "Members Remaning: " << endl;

		for (int i = 0; i < player.size(); i++)
		{
			cout << "[" << enemy[i]->getSide() << "] " << enemy[i]->getName() << " [" << enemy[i]->getHp() << "/" << enemy[i]->getMaxHp() << "]" << endl;
		}
	}

	else if (enemy.size() == 0) // If Enemy Team Wiped Out
	{
		cout << "Members Remaining: " << endl;

		for (int i = 0; i < player.size(); i++)
		{
			cout << "[" << player[i]->getSide() << "] " << player[i]->getName() << " [" << player[i]->getHp() << "/" << player[i]->getMaxHp() << "]" << endl;
		}
	}
}

int main()
{
	srand(time(0));

	vector<unit*> player;
	vector<unit*> enemy;
	vector<unit*> characters;

	playerSummon(player);
	enemySummon(enemy);
	combineParty(player, enemy, characters);
	turns(characters);

	while (player.size() > 0 && enemy.size() > 0)
	{
		int playerChoice;
		int enemyChoice = rand() % 2 + 1;
		int enemyTarget = rand() % 2;

		cout << "========================" << endl;
		cout << "       Team Pasado      " << endl;
		cout << "========================" << endl;
		playerParty(player);

		cout << "========================" << endl;
		cout << "       Team Bagsak      " << endl;
		cout << "========================" << endl;
		enemyParty(enemy);

		system("pause");
		system("cls");

		printOrder(characters);

		system("pause");
		system("cls");

		cout << "========================" << endl;
		cout << "       Attacking        " << endl;
		cout << "========================" << endl;
		characters.at(0)->printStats();

		if (characters.at(0)->getSide() == "Pasado")
		{
			cout << "========================" << endl;
			cout << "        Command         " << endl;
			cout << "========================" << endl;
			cout << "[1]. Attack " << endl;
			cout << "[2]. Skill " << endl;
			cout << "Action: "; 
			cin >> playerChoice;
			playerAttack(player, enemy, characters, playerChoice);
		}
		else if (characters.at(0)->getSide() == "Enemy")
		{
			enemyAttack(player, enemy, characters, enemyChoice, enemyTarget);
		}

		system("pause");
		system("cls");

		checkPlayerHP(player);
		checkEnemyHP(enemy);
		afterTurn(characters);
	}

	ending(player, enemy);
	player.clear();
	enemy.clear();
	characters.clear();
	
	system("pause");
	return 0;
}