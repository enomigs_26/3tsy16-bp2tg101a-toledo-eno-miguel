#pragma once
#include "Materia.h"
#include <vector>
#include <string>

class Accessory
{
public:
	Accessory(std::string name);
	~Accessory();

	std::string getName();

private:
	std::string mName;
};
