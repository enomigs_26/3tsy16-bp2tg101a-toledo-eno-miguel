#include "Unit.h"

Unit::Unit(std::string name)
{
}
Unit::~Unit()
{
}

//Basic
std::string Unit::getName()
{
	return std::string();
}
int Unit::getLevel()
{
	return 0;
}
int Unit::getHp()
{
	return 0;
}
int Unit::getHpMax()
{
	return 0;
}
int Unit::getMp()
{
	return 0;
}
int Unit::getMpMax()
{
	return 0;
}
int Unit::getCurrentExp()
{
	return 0;
}
void Unit::addExp(int exp)
{
}
int Unit::getNextExp()
{
	return 0;
}

void Unit::addHp(int amount)
{
}
void Unit::takeDamage(int damage)
{
}
void Unit::heal(int amount)
{
}

//Equipment
Weapon * Unit::getWeapon()
{
	return nullptr;
}
Weapon * Unit::equipWeapon(Weapon * weapon)
{
	return nullptr;
}
Armor * Unit::getArmor()
{
	return nullptr;
}
Armor * Unit::equipArmor(Armor * armor)
{
	return nullptr;
}
Accessory * Unit::getAccessory()
{
	return nullptr;
}
Accessory * Unit::equipAccessory(Accessory * accessory)
{
	return nullptr;
}

//Items
void Unit::addItem(Item * item)
{
}
void Unit::useItem(Item * item)
{
}
void Unit::printInventory()
{
}

//Skills
void Unit::useSkill(Skill * skill)
{
}

//Primary Stats
int Unit::getStrength()
{
	return 0;
}
int Unit::getDexterity()
{
	return 0;
}
int Unit::getVitality()
{
	return 0;
}
int Unit::getMagic()
{
	return 0;
}
int Unit::getSpirit()
{
	return 0;
}
int Unit::getLuck()
{
	return 0;
}

//Derived Stats
int Unit::getAttack()
{
	return 0;
}
float Unit::getAttackPercent()
{
	return 0.0f;
}
int Unit::getDefense()
{
	return 0;
}
float Unit::getDefensePercent()
{
	return 0.0f;
}
int Unit::getMagicAtk()
{
	return 0;
}
int Unit::getMagicDef()
{
	return 0;
}
float Unit::getMagicDefPercent()
{
	return 0.0f;
}

void Unit::levelUp()
{
}
