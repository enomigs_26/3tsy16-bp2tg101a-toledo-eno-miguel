#pragma once
#include <string>
#include "Unit.h"
#include <iostream>

using namespace std;

class Unit;

class Skill
{
public:
	Skill(std::string name, int mpCost);
	~Skill();

	std::string getName();
	virtual bool activate(Unit* actor, Unit* target);


private:
	std::string mName;
	int mMpCost;
};