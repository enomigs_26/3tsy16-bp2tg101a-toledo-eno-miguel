#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>

using namespace std;

void wager(int &money, int &bet, int round)
{
	cout << "Round " << round << " of 10" << endl;
	cout << "Place your bet: ";
	cin >> bet;

	while (bet < 100 || bet > money)
	{
		cout << "Round " << round << " of 10" << endl;
		cout << "Place your bet: ";
		cin >> bet;
	}
	money -= bet;
}
void payout(int &money, int result, int &bet)
{
	if (result == 1)
	{
		cout << "You WON!" << endl;
		money = money + bet * 2;
	}
	else
	{
		cout << "You lost..." << endl;
	}
}
void playround(int &money, int &bet)
{
	int result = rand() % 2;

	payout(money, result, bet);
	
	system("pause");
	system("cls");
}
struct Roll
{
	int dice[3];
};
Roll* rollDice()
{
	Roll* roll = new Roll;

	(*roll).dice[0] = rand() % 6 + 1;
	(*roll).dice[1] = rand() % 6 + 1;
	(*roll).dice[2] = rand() % 6 + 1;

	return roll;
};
void printRoll(Roll *rollDice)
{
	cout << rollDice << endl;
}

int main()
{
	srand(time(0));
	int money = 90000;
	int bet = 0;

	for (int round = 1; round < 11; round++)
	{

		cout << "Money in da bank: " << money << endl;
		wager(money, bet, round);
		playround(money, bet);
		cout << "You now have: " << money << endl;
		system("pause");
		system("cls");
		if (money < 100)
		{
			cout << "You are out of money SOOOOOON!" << endl;
			break;
		}
	}

	system("pause");
	return 0;
}