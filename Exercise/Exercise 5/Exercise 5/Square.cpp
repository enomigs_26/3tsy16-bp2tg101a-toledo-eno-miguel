#include <iostream>
#include "Square.h"
#include "Shape.h"

using namespace std;

square::square(float side)
{
	side = side;
	shape::numSides = 4;
	shape::mArea = (side*side);
	
}

void square::printStats()
{
	shape::printStats();
	cout << "Area of Square: " << shape::mArea << endl;
	cout << "Number of sides: " << shape::numSides << endl;
}


