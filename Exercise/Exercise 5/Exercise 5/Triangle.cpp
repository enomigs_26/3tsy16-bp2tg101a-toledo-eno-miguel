#include <iostream>
#include "Shape.h"
#include "Triangle.h"

using namespace std;

triangle::triangle(float base, float height)
{
	mbase = base;
	mheight = height;
	shape::numSides = 3;
	shape::mArea = (0.5 * base * height);
}

void triangle::printStats()
{
	shape::printStats();
	cout << "Area of Triangle: " << shape::mArea << endl;
	cout << "Number of sides: " << shape::numSides << endl;
}
