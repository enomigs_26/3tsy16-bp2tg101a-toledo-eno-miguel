#include <iostream>
#include "Circle.h"
#include "Shape.h"

using namespace std;

circle::circle(float radius)
{
	radius = radius;
	shape::numSides = 0;
	shape::mArea = (3.14f*radius*radius);
}

void circle::printStats()
{
	shape::printStats();
	cout << "Area of Circle: " << shape::mArea << endl;
	cout << "Number of sides: " << shape::numSides << endl;
}
