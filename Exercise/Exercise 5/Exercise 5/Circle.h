#pragma once
#include "Shape.h"

class circle : public shape
{
public:
	circle(float radius);
	void printStats();
private:
	float radius;
};