#pragma once
#include "Shape.h"

class square : public shape
{
public:
	square(float side);
	void printStats();
private:
	float side;
};