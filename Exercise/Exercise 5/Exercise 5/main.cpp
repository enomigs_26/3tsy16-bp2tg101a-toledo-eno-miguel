#include "Shape.h"
#include "Circle.h"
#include "Square.h"
#include "Triangle.h"
#include <iostream>

using namespace std;

int main()
{
	triangle triangleObjects(5, 10);
	square squareObjects(3);
	circle circleObjects(3);

	triangleObjects.printStats();
	squareObjects.printStats();
	circleObjects.printStats();

	system("pause");
	return 0;
}