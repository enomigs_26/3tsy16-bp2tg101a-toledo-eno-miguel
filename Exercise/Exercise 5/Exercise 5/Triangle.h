#pragma once
#include "Shape.h"

class triangle : public shape
{
public:
	triangle(float base, float height);
	void printStats();
private:
	float mbase;
	float mheight;
};